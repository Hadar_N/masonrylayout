const picNum = 30;
const columnNum = 5;
const $ = (className, containerObj = document) => containerObj.querySelector(className);

let howMany = {big: 0, small: 0};
let columnsForGrid;

const imgContainer = $(".photos-containter");
const body = $("body");

function getMeta(url){
    return new Promise ((res, rej) => {
    var img = new Image();
    img.src = url;
    img.onload = function(){
        res( this.width / this.height );
    };
    img.onerror = rej;
    })
}   

const createPictures = async () => {
    for (let i = 0; i < picNum; i++) {
        // console.log(howMany);
        if (howMany.big >= howMany.small) {
            columnsForGrid = 2;
        } else {
            columnsForGrid = Math.ceil(columnNum / 2);
        }

        const img = document.createElement("img");
        const imgColumns = Math.floor(Math.random() * (columnsForGrid)) + 1;

        if (imgColumns >= (columnNum-1)/ 2) howMany.big++;
        else howMany.small++;

        const url = (await fetch(`https://source.unsplash.com/random?sig=${i}`)).url//.then(res => res.blob()).then((res) => URL.createObjectURL(res));

        getMeta(url).then((proportion)=>{
        // console.log(proportion, imgColumns, proportion*imgColumns);

        img.src= url;
        img.classList.add("photos-containter__img");
        img.setAttribute("style", `grid-column: span ${imgColumns}; grid-row: span ${Math.round(proportion*imgColumns)}`);
        imgContainer.appendChild(img);

    })
    }

}

createPictures();